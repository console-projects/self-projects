package Self_projects;

import java.util.Scanner;

public class Atm_methods {
	static Scanner sc = new Scanner(System.in);
	static int pin = 1998;
	static int amount = 8000;
	int pinNumber;

//Menu	
	public void Menu() {
		System.out.println("\nWithdraw\tpress: 1\nAdd cash\tpress: 2\nBalance\t\tpress: 3\nPin change\tpress: 4");

		System.out.println("\nSelect your option:");
		int opt = sc.nextInt();
		if (opt < 5) {

			if (opt == 1) {
				withdraw();
			} else if (opt == 2) {
				Add_cash();
			} else if (opt == 3) {
				Balance();
			} else if (opt == 4) {
				pin_change();
			}
		} else {
			System.out.println("You have entered wrong number");
		}
	}

//1. withdraw method
	public void withdraw() {
		System.out.println("Enter the amount");
		int get_amount = sc.nextInt();
		amount -= get_amount;
		if (amount > 0) {
			System.out.println("Your trasaction is succefully completed");
			System.out.println("Your account Balance : Rs." + amount);
		} else {
			System.out.println("Insufficient Balance");
		}
	}

//2. Add cash
	public void Add_cash() {
		System.out.println("Enter the amount");
		int add_amount = sc.nextInt();
		amount += add_amount;
		System.out.println("Your trasaction is succefully completed");
		System.out.println("Your account Balance : Rs." + amount);
	}

//3.Balance enquiry
	public void Balance() {
		System.out.println("Your account Balance : Rs." + amount);
	}

//4.Pin change
	public void pin_change() {
		System.out.println("Enter new pin number");
		int new_pin = sc.nextInt();
		pinNumber = new_pin;
		System.out.println("pinNumber "+pinNumber);
		System.out.println("Your pin number has succefully changed");
	}
	
	public static void main(String[] args) {
		Atm_methods obj = new Atm_methods();

		System.out.println("Enter your Pin number");
		int pin_number = sc.nextInt();
		if(pin_number==pin) {
			obj.Menu();
			while (true) {
				System.out.println("\n\nIf you want to continue Press:1");
				System.out.println("If you want to EXIT Press:0");
				int choice = sc.nextInt();
					
				if (choice == 1) {
					obj.Menu();
				} else if (choice > 1) {
					System.out.println("You have entered wrong number");
				} else {
					System.out.println("Thank You");
					break;
				} // else
			} // while
		} // if

		else {
			System.out.println("Invalid pin number");
		}
	}

}
