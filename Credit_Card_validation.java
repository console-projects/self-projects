package Projects;     // Luhn Algorithm  //

import java.util.Arrays;

public class Credit_Card_validation {

	public static void main(String[] args) {
		
		int[] no= {7,9,9,2,7,3,9,8,7,1,3};
		
		for(int i=1;i<no.length;i+=2)
		{
			int value=no[i]*2;
			
			if(value>10)
			{
				int sum=(value%10)+(value/10);  
				no[i]=sum;
			}
			else
				no[i]=value;
		}
		
		System.out.println(Arrays.toString(no));
		
		int sum=0;
		for(int i=0;i<no.length;i++)
		{
			sum=sum+no[i];  
		}
	
		if(sum%10==0)
			System.out.println("The credit card number is valid");
		else
			System.out.println("The credit card number is Invalid");

	}

}
