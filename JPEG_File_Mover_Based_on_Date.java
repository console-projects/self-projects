package Projects_muthu_sir;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Time_Stamp {

	public static void main(String[] args) {
		
		        Path sourceDir = Paths.get("/home/sivakumar/Documents/Photos");  // Source directory path
		        Path ooty = Paths.get("/home/sivakumar/Documents/Photos/ooty");  // Destination directory path
		        Path kodai = Paths.get("/home/sivakumar/Documents/Photos/kodai");
		   
		 String targetDate1 = "2024-01-23";  // Target date in "yyyy-MM-dd" format
		 String targetDate2 = "2024-03-15"; 
		 
		 TransferFiles(targetDate1,sourceDir,ooty);
		 TransferFiles(targetDate2,sourceDir,kodai);	 

	}
		 
	
	public static void TransferFiles(String targetDate,Path sourceDir,Path destDir) {
			 
	        try {
	            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	            Date target = sdf.parse(targetDate);

	            Files.walkFileTree(sourceDir, new SimpleFileVisitor<Path>() {
	                @Override
	                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
	                    if (Files.isRegularFile(file) && isTargetDate(file, target) && (file.toString().toLowerCase().endsWith(".jpg") || file.toString().toLowerCase().endsWith(".jpeg")))
	                    {
	                        moveFile(file, destDir);
	                    }
	                    return FileVisitResult.CONTINUE;
	                }
	            });
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }

	    private static boolean isTargetDate(Path file, Date targetDate) throws IOException {
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        Date fileDate = new Date(Files.getLastModifiedTime(file).toMillis());
	        String formattedFileDate = sdf.format(fileDate);
	        String formattedTargetDate = sdf.format(targetDate);
	        return formattedFileDate.equals(formattedTargetDate);
	    }

	    private static void moveFile(Path sourceFile, Path destDir) throws IOException {
	        Path destFile = destDir.resolve(sourceFile.getFileName());
	        Files.move(sourceFile, destFile, StandardCopyOption.REPLACE_EXISTING);
	        System.out.println("Moved: " + sourceFile.toString() + " to " + destFile.toString());
	    
	    }
	}


