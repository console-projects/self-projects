package exeption_demo;

import java.util.Scanner;

public class Password_validation {

	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter password without using any space");
		
		String password=sc.next();   /*next()-- input kudukrapo space eduthukadhu,  nextLine()--space ohyum sethu eduthukum*/
		
		password_validation(password);
		
	}

	private static void password_validation(String password) {
		if(password.length()>=8)
		{
			if(password.charAt(0)>='A' && password.charAt(0)<='Z')
			{
				int small_ch=0, spl_ch=0, num=0;
				for(int i=1;i<password.length();i++)
				{
					if(password.charAt(i)>='a' && password.charAt(i)<='z')
					{
						small_ch++;
					}
					else if(password.charAt(i)>='0' && password.charAt(i)<='9')
					{
						num++;
					}
					else
					{
						spl_ch++;
					}
				}
				if(spl_ch==0)
				{
					spcl_ch_missing scme=new spcl_ch_missing();
					throw scme;
				}
				else if(num==0)
				{
					Number_missing nm=new Number_missing();
					throw nm;
				}
//				System.out.println("small letter-- "+small_ch);
//				System.out.println("Special characters---"+spl_ch);
//				System.out.println("Numbers----"+num);
				System.out.println("Valid password");
			}
			else
			{
				No_caps_exception nce=new No_caps_exception();
				throw nce;
			}
		}
		else
		{
			Pass_length_miss_match_Exception plme=new Pass_length_miss_match_Exception();
			throw plme;
		}
		
	}

}
