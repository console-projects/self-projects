package Projects_muthu_sir;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class IPL {

	public static void main(String args[]) {

		String[] Bowlers = new String[10];
		int[] Wickets = new int[10];

		try {
			// Specify the path to the Excel file
			FileInputStream file = new FileInputStream("/home/sivakumar/Desktop/Project-1/IPL.xlsx");

			// Create a workbook instance
			XSSFWorkbook workbook = new XSSFWorkbook(file);

			System.out.println("BOWLERS AND WICKETS\n");

				XSSFSheet sheet = workbook.getSheetAt(4);
				
				
				 for (int i =1; i <sheet.getLastRowNum(); i++) {
					
		                Row row = sheet.getRow(i);
		              
		                try {
		                int value = (int) row.getCell(1).getNumericCellValue();  // wickets in cell to convert value
		              
		                if(row.getCell(0)!=null)
		                {
		                	  System.out.println(row.getCell(0) + "\t" + value);  
		                	  
		                		  Bowlers[i-1]=row.getCell(0).toString();  //bcz for each loop start in 1	                		  
		                		  Wickets[i-1]=value;                	    
		                }
		                else 
		                {
		                	break;
		                }
		                } 
		                catch (NullPointerException e ) {
						 }
		            }

		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		
//		System.out.println(Arrays.toString(Bowlers));
//		System.out.println(Arrays.toString(Wickets));
		
		int index=0;
		int max=0;
		for (int i =0; i <Wickets.length; i++) 
		{
			if(Wickets[i]>max)
			{
				max=Wickets[i];
				index=i;
			}
		}
		System.out.println("\nThe highest wicket taker is");
		System.out.println(Bowlers[index] + "\t" + max);
					
				
	}
}
